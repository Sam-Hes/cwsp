
#ifndef MOVE_H_
#define MOVE_H_
#include "location.h"
#include "product.h"
#include <cmath>
#include <cstdlib>

class crane;

class request
{
public:
	request();
	~request();
	int left();
	int right();
	int otherCraneId();
	double duration();
	void setTimes(double start, double end);

	int id;
	int craneId;

	double startTime;
	double endTime;
	double delay;

	bool need_la;
	std::string type;
	product* product_ptr;
	location* source;
	location* destination;

	std::vector<request*> precedences;
};

#endif