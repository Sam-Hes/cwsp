#ifndef GRAPH_H
#define GRAPH_H

#include "problem.h"
#include <list>
#include <stack>

class graph{
public:
    graph();
    graph(problem * p);
    ~graph();

    problem *Problem;

    int V;

    std::vector<product *> sortedProducts;
    std::vector<std::list<int>> adj;

    void addEdge(int v, int w);
    void topologicalSortUtil(int v, bool visited[], std::stack<int> &Stack);
    void topologicalSort();
    int getProductIndex(int);
};
#endif