#include "problem.h"
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <utility>
#include <sstream>

using boost::property_tree::ptree;

bool sortRelTime(product *a, product *b) {
    return a->releaseTime < b->releaseTime;
}

problem::problem() = default;

problem::problem(std::string str, double alpha, double beta) {
    this->name = std::move(str);
    this->alpha = alpha;
    this->beta = beta;
    fromFile();
}

problem::~problem() = default;

void problem::fromFile() {

    std::stringstream ss;
    ss << name;

    ptree pt;
    boost::property_tree::read_json(name, pt);

    std::map<std::string, int> product_map;

    // reading the parameters
    const auto p = pt.get_child("parameters");
    this->craneNr = p.get<int>("number_of_cranes");
    this->CST = p.get<int>("crane_safety_time");
    this->stackingLevel = p.get<int>("stacking_level");
    Blocks.reserve(pt.get_child("blocks").size());

    for (auto &b : pt.get_child("blocks")) {
        location B;
        B.id = b.second.get<int>("id");
        B.xcor = b.second.get<int>("xcor");
        B.ycor = b.second.get<int>("ycor");

        for (auto &bb : b.second.get_child("times_to_blocks")) {
            B.BtoB.push_back(bb.second.get<int>(""));
        }

        if (b.second.get<std::string>("type") == "yard") {

            B.type = "yard";
            B.zcor = b.second.get<int>("zcor");
            if (B.zcor == stackingLevel-1)
                B.aboveBlock = nullptr;
            Blocks.push_back(B);
            yardBlocks.push_back(&Blocks.back());
        }
        else {
            B.type = "io";
            B.zcor = 0;
            Blocks.push_back(B);
            ioBlocks.push_back(&Blocks.back());
        }
    }

    Products.reserve(pt.get_child("products").size());

    for (const auto &p : pt.get_child("products")) {
        product P;
        P.id = p.second.get<int>("id");

        if (p.second.get<std::string>("type") == "yard") {
            P.type = "yard";
            P.blockId = &Blocks[p.second.get<int>("block_id")];
            P.source_block = &Blocks[p.second.get<int>("block_id")];
            P.destination_block = &Blocks[p.second.get<int>("block_id")];

            Products.push_back(P);
            yardProducts.push_back(&Products.back());
        }
        else if (p.second.get<std::string>("type") == "output") {
            P.type = "output";
            P.blockId = nullptr;
            P.source_block = &Blocks[p.second.get<int>("source_block_id")];
            P.destination_block = &Blocks[p.second.get<int>("dest_block_id")];
            P.releaseTime = p.second.get<double>("rel_time");
            P.dueTime = p.second.get<double>("due_time");

            if (p.second.get_child_optional("precedence")) {
                for (auto &pp : p.second.get_child("precedence")) {
                    P.precedence.push_back(pp.second.get<int>(""));
                }
            }

            Products.push_back(P);
            oProducts.push_back(&Products.back());
            ioProducts.push_back(&Products.back());
            productsMap[P.id] = &Products.back();
        }
        else {
            P.type = p.second.get<std::string>("type");
            P.source_block = &Blocks[p.second.get<int>("source_block_id")];
            P.releaseTime = p.second.get<double>("rel_time");
            P.dueTime = p.second.get<double>("due_time");
            P.blockId = nullptr;
            P.destination_block = nullptr;

            for (auto &pp : p.second.get_child("costs_proximity_products")) {
                P.PonPCosts.push_back(pp.second.get<int>(""));
            }

            if (p.second.get_child_optional("precedence")) {
                for (auto &pp : p.second.get_child("precedence")) {
                    P.precedence.push_back(pp.second.get<int>(""));
                }
            }

            Products.push_back(P);
            iProducts.push_back(&Products.back());
            ioProducts.push_back(&Products.back());
            productsMap[P.id] = &Products.back();
        }
    }
    std::sort(ioProducts.begin(), ioProducts.end(),
              sortRelTime);

    for (auto &b : pt.get_child("blocks")) {
        auto b_id = b.second.get<int>("id");
        if (b.second.get<std::string>("type") == "yard") {
            if (b.second.get_optional<int>("above_block_id"))
                Blocks[b_id].aboveBlock = &Blocks[b.second.get<int>(
                        "above_block_id")];

            if (b.second.get_optional<int>("product_id"))
                Blocks[b_id].product_ptr = &Products[b.second.get<int>(
                        "product_id")];
            else
                Blocks[b_id].product_ptr = nullptr;

            if (b.second.get_child_optional("underneath_blocks")) {
                for (auto &bb : b.second.get_child("underneath_blocks")) {
                    Blocks[b_id].underneathBlocks.push_back(
                            &Blocks[bb.second.get<int>("")]);
                }
            }
            for (auto &bb : b.second.get_child("neighbour_blocks")) {
                Blocks[b_id].neighbors.push_back(
                        &Blocks[bb.second.get<int>("")]);
            }
        }
    }
}

void problem::print(std::string str) {
    int number_of_cranes = 2;
    //std::string str = "new_outputs/instances" + name;

    using boost::property_tree::ptree;
    {
        ptree pt;

        //parameters
        ptree parameters;
        parameters.put("number_of_cranes", number_of_cranes);
        parameters.put("crane_safety_time", CST);
        //parameters.put("yard_length", yardLength + 2);
        //parameters.put("yard_width", yardWidth + 2);
        parameters.put("stacking_level", stackingLevel);
        //parameters.put("alpha", alpha);
        //parameters.put("beta", beta);
        //parameters.put("gamma", gamma);
        pt.add_child("parameters", parameters);


        //position slots
        ptree blocks;
        for (auto& b : Blocks){
            ptree block;
            {
                block.put("id", b.id);
                block.put("type", b.type);
                block.put("xcor", b.xcor);
                block.put("ycor", b.ycor);
                if (b.type == "yard")
                {
                    block.put("zcor", b.zcor);
                    if (b.product_ptr != nullptr)
                    {
                        block.put("product_id", b.product_ptr->id);
                    }
                    if (b.aboveBlock != nullptr)
                    {
                        block.put("above_block_id", b.aboveBlock->id);
                    }
                    ptree neighbours;
                    for (auto & n : b.neighbors)
                    {
                        ptree neighbour;
                        neighbour.put("", n->id);
                        neighbours.push_back(std::make_pair("", neighbour));
                    }
                    block.add_child("neighbour_blocks", neighbours);

                    if (b.zcor != 0)
                    {
                        ptree underneath_blocks;
                        for (auto & u : b.underneathBlocks)
                        {
                            ptree underneath_block;
                            underneath_block.put("", u->id);
                            underneath_blocks.push_back(std::make_pair("", underneath_block));
                        }
                        block.add_child("underneath_blocks", underneath_blocks);
                    }
                }
                ptree times;
                for (auto & t : b.BtoB)
                {
                    ptree time;
                    time.put("", t);
                    times.push_back(std::make_pair("", time));
                }
                block.add_child("times_to_blocks", times);
            }
            blocks.push_back(std::make_pair("", block));
        }
        pt.add_child("blocks", blocks);

        //products
        ptree products;
        for (auto & p : Products){
            if (p.type != "input") {
                ptree prod;
                {

                    prod.put("id", p.id);
                    prod.put("type", p.type);
                    if (p.type == "yard") {
                        prod.put("block_id", p.blockId->id);

                    }
                    if (p.type == "output") {
                        prod.put("source_block_id", p.source_block->id);
                        prod.put("dest_block_id", p.destination_block->id);
                        prod.put("rel_time", p.releaseTime);
                        prod.put("due_time", p.dueTime);
                    }
                    if (p.type == "yard_movement") {
                        prod.put("source_block_id", p.source_block->id);
                        prod.put("rel_time", p.releaseTime);
                        prod.put("due_time", p.dueTime);
                        ptree pcosts;
                        for (auto &pc : p.PonPCosts) {
                            ptree pcost;
                            pcost.put("", pc);
                            pcosts.push_back(std::make_pair("", pcost));
                        }
                        prod.add_child("costs_proximity_products", pcosts);
                    }
                    if (p.precedence.size() != 0) {
                        ptree precedences;
                        for (auto &i : p.precedence) {
                            ptree precedence;
                            precedence.put("", i);
                            precedences.push_back(std::make_pair("", precedence));
                        }
                        prod.add_child("precedence", precedences);
                    }
                }
                //products.add_child(std::to_string(p.id), prod);
                products.push_back(std::make_pair("", prod));
            }
        }
        for (auto & p : iProducts)
        {
            ptree prod;
            {
                if (p->type == "input"){
                    prod.put("id", p->id);
                    prod.put("type", p->type);
                    prod.put("source_block_id", p->source_block->id);
                    prod.put("rel_time", p->releaseTime);
                    prod.put("due_time", p->dueTime);
                    ptree pcosts;
                    for (auto &pc : p->PonPCosts) {
                        ptree pcost;
                        pcost.put("", pc);
                        pcosts.push_back(std::make_pair("", pcost));
                    }
                    prod.add_child("costs_proximity_products", pcosts);

                }
            }
            products.push_back(std::make_pair("", prod));
        }
        pt.add_child("products", products);

        boost::property_tree::write_json(str, pt);// , std::locale(), false);
        //std::locale(), pretty);

    }
}

