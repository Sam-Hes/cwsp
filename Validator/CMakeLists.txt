cmake_minimum_required(VERSION 3.10)
project(Validator)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -lboost_system")

set(SOURCE_FILES
        main.cpp
        location.cpp
        location.h
        crane.cpp
        crane.h
        request.cpp
        request.h
        problem.cpp
        problem.h
        product.cpp
        product.h
        solution.cpp
        solution.h
        graph.h graph.cpp)


set(Boost_INCLUDE_DIR /usr/local/Cellar/boost/1.67.0_1/include)
set(Boost_LIBRARY_DIR /usr/local/Cellar/boost/1.67.0_1/lib)
find_package(Boost 1.67.0)
include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIR})

add_executable(Validator ${SOURCE_FILES})