#ifndef PRODUCT_H_
#define PRODUCT_H_
#include <string>
#include<vector>
class location;

class product
{
public:
	product();
	~product();

	std::vector<int> precedence;
	std::vector<int> PonPCosts;
	//std::vector<int> PinBcosts;

	std::string type;

	int id;
	location* blockId;
	location* source_block;
	location* destination_block;
	double releaseTime;
	double dueTime;
};
#endif