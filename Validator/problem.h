
#ifndef PROBLEM_H_
#define PROBLEM_H_

#include <vector>
#include <map>
#include <string>
#include <tuple>
#include <iostream>
#include <sstream>
#include <fstream>
#include "product.h"
#include "location.h"
#include "crane.h"

class problem
{
public:
	problem();
	problem(std::string str, double alpha, double beta);
	~problem();

	void fromFile();
	void print(std::string str);

	int CST; //crane safety time
	int craneNr;
	int stackingLevel;
	double alpha;
	double beta;
	std::string name;

	std::vector<location> Blocks;
	std::vector<location*> yardBlocks;
	std::vector<location*> ioBlocks;

	std::vector<product> Products;

	std::vector<product*> yardProducts;
	std::vector<product*> ioProducts;
	std::vector<product*> iProducts;
	std::vector<product*> oProducts;

	std::map<int, product*> productsMap;
};

#endif