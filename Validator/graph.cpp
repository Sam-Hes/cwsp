#include "graph.h"
#include <queue>

graph::graph() = default;

graph::graph(problem * p) {
    Problem = p;
    V = (int) p->ioProducts.size();
    sortedProducts.reserve(p->ioProducts.size());
    adj.resize(V);

   for(auto & prod : p->ioProducts ){
       for(auto & prec : prod->precedence){
           addEdge(getProductIndex(prec),getProductIndex(prod->id));
       }
   }
}

graph::~graph() = default;

void graph::addEdge(int v, int w)
{
    adj[v].push_back(w); // Add w to v’s list.
}

void graph::topologicalSortUtil(int v, bool *visited, std::stack<int> &Stack) {
    visited[v] = true;

    std::list<int>::iterator i;
    for (i = adj[v].begin(); i != adj[v].end(); ++i)
        if (!visited[*i])
            topologicalSortUtil(*i, visited, Stack);

    Stack.push(v);

}

void graph::topologicalSort()
{

    std::vector<int> in_degree(V, 0);

    for (int u=0; u<V; u++)
    {
        std::list<int>::iterator itr;
        for (itr = adj[u].begin(); itr != adj[u].end(); itr++)
            in_degree[*itr]++;
    }

    std::queue<int> q;
    for (int i = 0; i < V; i++)
        if (in_degree[i] == 0)
            q.push(i);

    int cnt = 0;
    std::vector <int> top_order;
    while (!q.empty())
    {
        int u = q.front();
        q.pop();
        top_order.push_back(u);
        std::list<int>::iterator itr;
        for (itr = adj[u].begin(); itr != adj[u].end(); itr++)

            if (--in_degree[*itr] == 0)
                q.push(*itr);

        cnt++;
    }

    for (int i : top_order)
        sortedProducts.push_back(Problem->ioProducts[i]);

}

int graph::getProductIndex(int id){
    int i = 0;
    for (auto & p :Problem->ioProducts){
        if (id == p->id)
            return i;
        i++;
    }
}