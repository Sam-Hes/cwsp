
#ifndef CRANE_H_
#define CRANE_H_

#include <vector>
#include "location.h"
#include "request.h"

class crane{
public:
	crane();
	~crane();

	int id;
	location* currentBlock;
	std::vector<product*> schedule;
	std::vector<request*> moves;
};

#endif